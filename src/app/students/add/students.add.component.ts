import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})

export class StudentsAddComponent implements OnInit {
  students: Student[];
  form = this.fb.group({
    id: [''],
    studentId: [''],
    name: [''],
    surname: [''],
    gpa: [''],
    image: [''],
    featured: [''],
    penAmount: [''],
    description: [''],
  });

  validation_messages = {
    'studentId': [
      { type: 'required', message: 'student id is required' },
      { type: 'maxLength', message: 'student id is too long' },
      { type: 'pattern', message: 'please enter number' },
    ],
    'name': [
      { type: 'required', message: 'name is required' },
    ],
    'surname': [
      { type: 'required', message: 'surname is required' },
    ],
    'penAmount': [
      { type: 'required', message: 'penAmount is required' },
      { type: 'pattern', message: 'please enter number' },
    ],
    'image': [],
    'gpa': [
      { type: 'pattern', message: 'please enter a 2 digits decinal number' },
    ],
    'description': []
  }

  constructor(private fb: FormBuilder, private studentService: StudentService, private router: Router) {

  }

  getErrorMessage() {
    if (this.form.get('studentId').value) {
      if (this.form.get('studentId').value > 10) {
        return 'student id is too long';
      }
      if (this.form.get('studentId').value.length != 0) {
        return '';
      }
    }
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      studentId: [null, Validators.compose(
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.pattern('[0-9]+')
        ]
      )],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      image: [null],
      penAmount: [null, Validators.compose(
        [
          Validators.required,
          Validators.pattern('[0-9]+')
        ]
      )],
      description: [null],
      gpa: [null, Validators.compose(
        [
          Validators.required,
          Validators.pattern('[0-9]+.[0-9][0-9]')
        ]
      )]
    });


  }
  upQuantity(student: Student) {
    this.form.patchValue({
      penAmount: +this.form.value['penAmount'] + 1
    })
  }

  downQuantity(student: Student) {
    if (+this.form.value['penAmount'] > 0) {
      this.form.patchValue({
        penAmount: +this.form.value['penAmount'] - 1
      })
    }
  }

  submit() {
    this.studentService.saveStudent(this.form.value)
      .subscribe((student) => {
        this.router.navigate(['/detail', student.id]);
      }, error => {
        alert('Could not save value!');
      })
  }

}
